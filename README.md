**Note: This is a first draft, nothing here is set in stone, we will discuss this on the company seminar to get everyone on board :)**
---


# Introduction
These are the style guidelines and best practices for writing code for 2-.0 LCA Consultants. Please make sure to read them carefully before commiting any code to one of the code repositories.

## Table of content
1. Coding Best Practices
2. How to use git?
3. Python
4. List of programming enviornments we use
5. Contributing to this document

# Coding Best Practices

There are several places where you can find good guidelines. And to stay in line with a first remark

---
        Don't do things others have already done for you
---

here are two articles well worth reading. A good introduction to coding if you come from a scientific background is [this paper](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745). And [this website](https://opensource.com/article/17/5/30-best-practices-software-development-and-testing) also has a very nice and consice list of things you can do to write good code. If you ever happen to write code that might have security implications (i.e. code that interacts with third parties). [This guide](https://owasp.org/www-pdf-archive/OWASP_SCP_Quick_Reference_Guide_v2.pdf) is a starting guide on how to write secure code.

We want to emphazise the following guidelines that should be adhered to when coding for projects of 2-.0 LCA Consultants.

1. **Use git if you have more than one programmer working on the project**

It will most likely make your life easier to also do this if you are working alone. The versioning system of git can be a real life saver if your code suddenly doesn't work anymore - simply look at the diff since your last commit and work out where it might went wrong.
Also keep in mind that maybe someone else could benefit from your code in the future. If the code resides on a central git repo it will be much easier to find.

Read the section 'How to use git' on instructions of how to set everything up.

2.  **Write test routines for your code**

This might seem counter-intuitive at first, but writing tests can save you a **lot** of time!

Once you know how to write tests in your programming language, it is very fast to set them up. Once you have them set-up you can easily run them over and over and over again. Usually you write tests that only take a fraction of a second to execute and it automates the process of running the program and checking whether it produced the right results for you.
If you are currently running your program every time you changed something to see if it still works, tests are for you!

Also if you encounter bugs along the way, simply makeing the bug into a test case will ensure that it never happens again.

Most programming languages have libraries to do unit testing. For python one tool to use is pytest. Look at 'How to use pytest' in this document for instructions how to use it.

3. **Write modular code**

Splitting code up in methods/classes and different files, makes it much easier to read the code and to understand its meaning.
It also makes it much easier to write tests if you have consise methods.

Finally, it will make it easier to reuse part of the code in other projects.
        
3. **Try to follow the coding style guide of the language you are using**

Style guides are there for a reason. You don't have to necessarilly follow the style guide 100% all the time, but try to do it as much as possible.
        
5. **Use descriptive variable names**

Try not to use `bpdve` as a variable name. Use `best_practices_descriptive_variable_example` instead. 
If you don't want to type out `best_practices_descriptive_variable_example` every time, make sure to use a programming environment that autocompletes variable names for you.

6. **Don't code if you are tired!**

This usually just creates a lot of bugs that will have to clean up later.

7. **Use .editorconfig in your repositories**

From their [website](https://editorconfig.org/):

EditorConfig helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs. The EditorConfig project consists of a file format for defining coding styles and a collection of text editor plugins that enable editors to read the file format and adhere to defined styles. EditorConfig files are easily readable and they work nicely with version control systems.

As an example, you could use the .editorconfig file from [python](https://github.com/python/cpython/blob/main/.editorconfig)

```
    root = true

    [*.{py,c,cpp,h,rst,md,yml}]
    trim_trailing_whitespace = true
    insert_final_newline = true
    indent_style = space

    [*.{py,c,cpp,h}]
    indent_size = 4

    [*.yml]
    indent_size = 2
```

8. **Use a .gitignore file in your repositories**

This will make sure that files that don't belong in the repo will never get there. For inspiration you can again look at the .gitignore file from [python](https://github.com/python/cpython/blob/main/.gitignore).

---

**If you are using git on a larger project, these additional best practices apply:**


9. **Create issues for bugs and suggestions**

Having bugs and suggestions tracked in the issues list of the repository helps to ensure that they are documented as close to the code as possible. It also helps others to find possible solutions to problems they might have by looking at already closed issues.


10. **Use pull requests to add new code**

Once more than one developer works on a specific repository, changes should only be added via pull requests (merge requests on GitLab).
This ensures that code can be reviewed by others before it is added to the code base.

To create pull requests, you have to fork the repository before working on it. Once you have forked your repository, git clone it onto your computer and start working.

Once you are finished with your task, and pushed your changes to your forked repository, you can then create a pull request. The pull request will be reviewed by the team and if everything looks good, the code is merged into the main repository.

Minor changes to documentation (e.g. removing typos in the readme) might be done without pull requests. But this option should be used only sparingly and pull requests are still prefered.


11. **Use pull request and issue templates for your repositories**

Using templates will make it easier for others to commit issues and pull requests.

You can find various issue and pull request templates at: https://github.com/stevemao/github-issue-templates

Templates can usually be set in the settings menu of the repository.


12. **If possible link your pull request to an issue**

It is good practice to link you pull request to an issue. This helps others see directly what you are contributing to the code base and also makes it easy to read up on any discussions in the issue tracker.

# How to use git?

If you have not worked with git before or want to freshen up your memory, [this](https://www.w3schools.com/git/default.asp) is a good place to start.
If you want to just have a list of FAQs for git. [Look here](https://www.git-tower.com/learn/git/faq/)

Here is a basic list to copy and paste for the most frequent things you do with git using the command line.

**Setting up a repository:**

If you have files on your pc and want make it a git repository:

        git init
        git remote add origin https://github.com/example/example.git
        git add *
        git commit -m "initial commit"
        git push origin main
        
If you want to use an exisiting repository:

        git clone https://github.com/example/example.git
        
Often you want to use ssh instead of https for the connection. Look [here](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) if you want to set it up.

**Pulling in new code before you push your changes**:

        git pull


**Checking the git status:**

        
        git status
        
**Adding files to be commited:**


        git add <path-to-file> <path-to-another-file>
        
You can use the * symbol to add multiple files at once. Note: deleted files are not added when using * . You still have to add them using their path.

        git add path/with/a/lot/of/changes/*
        
**Committing files:**

When you use `git commit` your default text editor will pop up so that you can add a commit message.
It is often easier and faster to use the `-m` flag to set it directly.

        git commit -m "A lot of nice changes"
        
**Creating a new branch:**

If you want to have a new branch based on your current base branch

        git branch <new-branch>
        
If you want to base your new branch on a different branch

        git branch <new-branch> <base-branch>
        
To create the new branch also in the remote repository use:

        git push -u origin <local-branch>
        
[More options and infos on creating branches](https://www.git-tower.com/learn/git/faq/create-branch/)

**Changing to a different branch:**

If you have a local branch you want to switch to:
        
        git checkout <branch-name>
        
If you want to switch to a remote branch:

        git checkout --track <remote-branch-name>
        e.g.:
        git checkout --track origin/develop

**Checking out an old commit:**

If you want to revert back to a previous commit.

        git checkout <commit-hash>
        e.g.:
        git checkout 757c47d4

Use `git log` to see the commit hashes

You can use:

        git checkout <branch-name>
        
to get back to the up-to-date changes.

## Tools
Git can be used from the command line directly. However, it might be more convenient to use a GUI application for managing branches/reverting to prior versions and doing all those other things possible with git. This section aims to provide a list of tools that are used in the company that you might want to give a try.

### GUI Tools
In case you don't want to use the command line, there are several tools out there that make working with git repos easy!

As with all tools, use the ones that you like and that make your life easier. Following is a list of the different tools that are used at 2-.0 LCA Consultants available (in no particular order).

1. **Command line (technically not a GUI)**

    (Users: Valentin Starlinger)

    Pros:

    + All you have to do to work with git on the command line, is to install git :)
    + Very lightweight in terms of memory usage and high speed
    + No extra software needed for purchase
    + All features of git right at your fingertips

    Cons:

    - Some tasks such as rebasing to previous commits or similar things might be slower and more tricky than with a GUI

2. **GitKraken?**

    (Users: )

    Pros:

    Cons:


### Pre-commit hooks
Git allows you to specify scripts that are executed before specific events happen when using git.
Take a close look at https://githooks.com/ if you want a deep dive :)

For development the most important git hook is arguably the pre-commit hook. Here you can run scripts that check if your code is ready for commiting.

You add scripts to hooks by adding them in the '.git/hooks/' folder in your local respository.

There are also libraries that make it easier to use them. An example is the python tool [pre-commit](https://pre-commit.com).
Once you have it installed you can simply put a .pre-commit-config.yaml file in your project path (next to your .gitignore), this will automatically run every time you commit changes to the git repo. If you push your .pre-commit-config.yaml to the repository, this will also make sure that everyone on your team uses the same pre-commit hooks.

Example [.pre-commit-config.yaml](examples/.pre-commit-config.yaml)

# Python
Most of the code we write at 2-.0 LCA Consultants is written in Python. This section tells you:

- How to setup your python enviornment so that you don't run into any issues
- How to build python packages
- Python coding style

If you are new to python, there are a ton of very good guides that you can read up on.
Here is a short list of articles you might find interesting:

- [The Hitchhiker's Guide to Python!](https://docs.python-guide.org/)
- [Programming for Computations A Gentle Introduction to Numerical Simulations with Python](http://hplgit.github.io/prog4comp/doc/pub/p4c_Python.pdf)
- [Getting to know pandas](https://pandas.pydata.org/docs/user_guide/index.html)
- [The definitive guide on how to use static, class or abstract methods in Python](https://julien.danjou.info/guide-python-static-class-abstract-methods/)

## Python Setup
Make sure to use virtual environments. You can set them up either using virtuanenv or conda.

[Why should I use virtual environments?](https://towardsdatascience.com/why-you-should-use-a-virtual-environment-for-every-python-project-c17dab3b0fd0)

### Using virtualenv

Most important commands:

        virtualenv -p python3 <path-where-you-want-your-python-env>
        e.g.:
        virtualenv -p python3 ~/.python_envs/test_env
    
You then activate the environment using:

        source <path-to-env>/bin/activate
        e.g.:
        source ~/.python_envs/test_env/bin/activate
        

### Using conda
(Also comes with a GUI!)

[Conda cheatsheet](https://docs.conda.io/projects/conda/en/latest/user-guide/cheatsheet.html)

Most important commands:

        conda create --name ENVNAME
        
Activate the environment

        conda activate ENVNAME
        
**when you are using conda you should try to avoid mixing pip installs and conda installs**

So use: 

        conda install pkg_name

## Building python packages
Creating a python package is easier than you might think. Especially since there are tools such as [pyscaffold](https://github.com/pyscaffold/pyscaffold/).

Using pyscaffold setting up a python package is as simple as writing the following code:

```
pip install pyscaffold

putup -i <name-of-your-package>
```

The -i option will launch a window with your set-up editor and lets you edit all available options with plenty of comments to read through in case you are unsure of what they are doing.

Python packages make it easy to distribute your code and use it in other projects.

## Coding Style Guide (PEP8)
PEP8 is a style guide for python. The official version can be found [here](https://www.python.org/dev/peps/pep-0008/) and [this](https://pep8.org/) is a prettier version of the same document. 

-----
Take your time to watch https://www.youtube.com/watch?v=wf-BqAjZb8M

Main takeaways from this talk:
Pythonic means "coding beautifully in harmony with the language to get the maximum benefits from Python"
Learn to recognize non-pythonic APIs and to recognize good code.  Don't get distracted by PEP 8.  Focus first an Pythonic versus NonPython (P vs NP). When needed, write an adapter class to convert from the former to the latter.

- Avoid unnecessart packageing in favor of simpler imports
- Create custom exceptions
- Use properties instaed of getter methods
- Create a context manager for recurring set-up and teardown logic
- Use magic methods:
          __len__ instead of getSize()
          __getitem__ instead of getRouteByIndex()
          make the table iterable
- Add good __repr__ for better dubuggability
 -----

You can use auto-formatters and linting to make your life much easier!

Popular choices include:

- [black](https://github.com/psf/black)
- [flake8](https://flake8.pycqa.org/en/latest/)
- [isort](https://pycqa.github.io/isort/)
- [sonarlint](https://www.sonarlint.org/)

Most programming environments offer an easy way to include them in your workflow.

# Our favorite programming environments
Everyone has their own preferences on how they want to code. In case you need inspiration or want to see who you can ask in case you have an issue with your software. These are the programming environments currently used by empolyees of 2-.0 LCA Consultants:

## [neovim](https://neovim.io/)
(Users: Valentin Starlinger)

neovim is the modern version of vim. It supports more plugins and can run scripts in the background. If you are currently using vim (or vi) I highly recommend changing :)

For a very quick overview of vim you can look at the wikipedia page https://en.wikipedia.org/wiki/Vim_(text_editor)


# Contributing to this document
If you found a typo, something that is confusing or lacking examples, please create an issue or a pull request to make this document even better for the future.